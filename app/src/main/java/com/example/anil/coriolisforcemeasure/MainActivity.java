package com.example.anil.coriolisforcemeasure;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class MainActivity extends AppCompatActivity implements SensorEventListener{

    //initializing variables
    SensorManager mSensorManager;
    Sensor mAccelerometerSensor;
    Sensor mGyroscopeSensor;

    //TextViews for XML display
    TextView mForceValueText;
    TextView mXAccValueText;
    TextView mYAccValueText;
    TextView mZAccValueText;
    TextView mXGyrValueText;
    TextView mYGyrValueText;
    TextView mZGyrValueText;
    TextView mXVelValueText;
    TextView mYVelValueText;
    TextView mZVelValueText;

    //variables for path of saving file
    final String DIR_SD = "MySensorData";
    String FILENAME_SD;

    //time variables to check at which time the sensor is accessed
    long gyroscopeTime;
    long accelerometerTime;

    Context context;

    Button start;
    Button stop;

    double velocity_x;
    double velocity_y;
    double velocity_z;

    double forceX;
    double forceY;
    double forceZ;

    long time;
    BufferedWriter bufferedWriter;
    SensorData sensorData;

    private class SensorData {

        //creating an ArrayList for all variables that will be saved to file
        public SensorData(){
            gyrX = new ArrayList<>();
            gyrY = new ArrayList<>();
            gyrZ = new ArrayList<>();
            accX = new ArrayList<>();
            accY = new ArrayList<>();
            accZ = new ArrayList<>();
            velY = new ArrayList<>();
            velX = new ArrayList<>();
            velZ = new ArrayList<>();
        }

        private ArrayList<String> gyrX, gyrY, gyrZ, accX, accY, accZ, velX, velY, velZ;


    }


    public MainActivity() throws FileNotFoundException {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        start = (Button)findViewById(R.id.button_start);
        stop = (Button)findViewById(R.id.button_stop) ;


        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> sensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        if (sensors.size() > 0) {
            for (Sensor sensor : sensors) {
                switch (sensor.getType()) {
                    case Sensor.TYPE_LINEAR_ACCELERATION:
                        if (mAccelerometerSensor == null) mAccelerometerSensor = sensor;
                        break;
                    case Sensor.TYPE_GYROSCOPE:
                        if (mGyroscopeSensor == null) mGyroscopeSensor = sensor;
                        break;
                    default:
                        break;
                }
            }
        }
        context= this;



        //bounding the XML and Java variables through R-class
        mForceValueText = (TextView)findViewById(R.id.value_force);
        mXAccValueText = (TextView)findViewById(R.id.value_x);
        mYAccValueText = (TextView)findViewById(R.id.value_y);
        mZAccValueText = (TextView)findViewById(R.id.value_z);
        mXGyrValueText = (TextView)findViewById(R.id.value_gyr_x);
        mYGyrValueText = (TextView)findViewById(R.id.value_gyr_y);
        mZGyrValueText = (TextView)findViewById(R.id.value_gyr_z);
        mXVelValueText = (TextView)findViewById(R.id.value_vel_x);
        mYVelValueText = (TextView)findViewById(R.id.value_vel_y);
        mZVelValueText = (TextView)findViewById(R.id.value_vel_z);

        //START BUTTON
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //create a file based on the current time
                time = System.currentTimeMillis();
                FILENAME_SD = String.valueOf(time) + ".html";
                sensorData = new SensorData();

                mSensorManager.registerListener(MainActivity.this, mAccelerometerSensor, SensorManager.SENSOR_DELAY_GAME);
                mSensorManager.registerListener(MainActivity.this, mGyroscopeSensor, SensorManager.SENSOR_DELAY_GAME);


            }

            });

        //STOP BUTTON
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSensorManager.unregisterListener(MainActivity.this);
                try {
                    createFileSD();
                } catch (Exception e){
                    e.printStackTrace();
                }

                //nullify all values
                mXGyrValueText.setText("0.0");
                mYGyrValueText.setText("0.0");
                mZGyrValueText.setText("0.0");
                mXVelValueText.setText("0.0");
                mYVelValueText.setText("0.0");
                mZVelValueText.setText("0.0");
                mXAccValueText.setText("0.0");
                mYAccValueText.setText("0.0");
                mZAccValueText.setText("0.0");
                mForceValueText.setText("0.0");

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }


    /**
     * Method called, when sensor value is changed
     * Communicates with the sensors and returns obtained values
     * @param event
     */
    public void onSensorChanged(SensorEvent event) {

        long currentTime;
        switch (event.sensor.getType()) {

            case Sensor.TYPE_GYROSCOPE:
                currentTime = System.currentTimeMillis();
                //to reduce error, update values only every 1 sec
                if (currentTime - gyroscopeTime > 1000) {

                    //obtaining values from gyroscope and convert them to String
                    String valueX = String.format(Locale.US, "%1.3f", event.values[SensorManager.DATA_X]);
                    String valueY = String.format(Locale.US, "%1.3f", event.values[SensorManager.DATA_Y]);
                    String valueZ = String.format(Locale.US, "%1.3f", event.values[SensorManager.DATA_Z]);

                    //Add the values to TextViews to display them
                    mXGyrValueText.setText(valueX);
                    mYGyrValueText.setText(valueY);
                    mZGyrValueText.setText(valueZ);

                    //Add values to ArrayList to save to file
                    sensorData.gyrX.add(valueX);
                    sensorData.gyrY.add(valueY);
                    sensorData.gyrZ.add(valueZ);

                    gyroscopeTime = currentTime;
                }
                break;


            case Sensor.TYPE_LINEAR_ACCELERATION:
                currentTime = System.currentTimeMillis();
                //to reduce error, update values only every 1 sec
                if (currentTime - accelerometerTime > 1000) {

                    //obtaining values from accelerometer and convert them to String
                    String accX = String.format(Locale.US, "%1.3f", event.values[SensorManager.DATA_X]);
                    String accY = String.format(Locale.US, "%1.3f", event.values[SensorManager.DATA_Y]);
                    String accZ = String.format(Locale.US, "%1.3f", event.values[SensorManager.DATA_Z]);

                    //Add the values to TextViews to display them
                    mXAccValueText.setText(accX);
                    mYAccValueText.setText(accY);
                    mZAccValueText.setText(accZ);

                    //Add values to ArrayList to save to file
                    sensorData.accX.add(accX);
                    sensorData.accY.add(accY);
                    sensorData.accZ.add(accZ);


                    //obtaining velocity
                    //to reduce the error, filter values, use only acceleration that exceeds 0.5
                    if (event.values[SensorManager.DATA_X] > 0.5 || event.values[SensorManager.DATA_X] < -0.5) {
                        mXVelValueText.setText((String.format(Locale.US, "%1.3f",
                                velocity_x += event.values[SensorManager.DATA_X])));
                    }
                    if (event.values[SensorManager.DATA_Y] > 0.5 || event.values[SensorManager.DATA_Y] < -0.5) {
                        mYVelValueText.setText((String.format(Locale.US, "%1.3f",
                                velocity_y += event.values[SensorManager.DATA_Y])));
                    }
                    if (event.values[SensorManager.DATA_Z] > 0.5 || event.values[SensorManager.DATA_Z] < -0.5) {
                        mZVelValueText.setText((String.format(Locale.US, "%1.3f",
                                velocity_z += event.values[SensorManager.DATA_Z])));
                    }

                    //add values to ArrayList to save to file
                    sensorData.velX.add(String.format(Locale.US, "%1.3f", velocity_x));
                    sensorData.velY.add(String.format(Locale.US, "%1.3f", velocity_y));
                    sensorData.velZ.add(String.format(Locale.US, "%1.3f", velocity_z));



                    //using the obtained data to measure the force.
                    //if-else is used to avoid NullPointer, if arrays are not yet filled with data
                    if (sensorData.gyrY.size()>2.0){
                        forceX = Double.valueOf(sensorData.gyrY.get(sensorData.gyrY.size() - 1))*velocity_z
                                - Double.valueOf(sensorData.gyrZ.get(sensorData.gyrZ.size() - 1))*velocity_y;

                        forceY = Double.valueOf(sensorData.gyrX.get(sensorData.gyrX.size() - 1))*velocity_z
                                - Double.valueOf(sensorData.gyrZ.get(sensorData.gyrZ.size() - 1))*velocity_x;

                        forceZ = Double.valueOf(sensorData.gyrX.get(sensorData.gyrX.size() - 1))*velocity_y
                                - Double.valueOf(sensorData.gyrY.get(sensorData.gyrY.size() - 1))*velocity_x;
                    }
                    else {
                        forceX = 0.0;
                        forceY = 0.0;
                        forceZ = 0.0;
                    }

                    //calculating the net force and displaying it
                    mForceValueText.setText(String.format(Locale.US, "%1.3f",Math.sqrt(0.133*(forceX*forceX+forceY*forceY+forceZ*forceZ))));
                    accelerometerTime = currentTime;
                }
                break;
        }
    }

    /**
     * Method asking for the permission to write to file.
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    public void onRequestPermissionsResult(int requestCode,  String[] permissions,  int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 123: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    start.performClick();
                } else {
                    //show a notification, if permission has not been granted
                    Toast.makeText(context, "Impossible to write data to the file", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    /**
     * Method used to create a file to write to
     */
    void createFileSD() {
        // check if SD is available
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return;
        }
        // get the path to SD
        File sdPath = Environment.getExternalStorageDirectory();
        // add new catalogue for files
        sdPath = new File(sdPath.getAbsolutePath() + "/" + DIR_SD);
        // create the catalogue
        sdPath.mkdirs();
        // create a File object based on the existing path
        File sdFile = new File(sdPath, FILENAME_SD);
        try {
            // open a write stream
            bufferedWriter = new BufferedWriter(new FileWriter(sdFile));
            // write data in Json format
            Gson gson = new Gson();
            String json = gson.toJson(sensorData);

            //add data and close the stream
            bufferedWriter.write(json);
            bufferedWriter.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
